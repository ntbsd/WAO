# WAO

1. 将所有文件解压到一个目录下面.
2. 双击打开“C++运行库安装.zip”压缩包文件,可以不用解压，里面所有exe安装程序都安装一遍，如果已经安装好了，可以跳过此步骤.
3. 双击打开“PhpStudy2018.exe”运行程序，使其解压到一个目录下面，目录路径要记住！
4. 找到刚才安装的目录路径，并且在当前目录下面找到“phpStudy.exe”运行程序，请双击打开它，并且允许以管理员身份运行.
5. 进入程序后，找到“切换版本”这一处选项，点击并且选择“php-7.2.10-nts + Nginx”这个选项.
6. 点击“其他选项菜单”按钮，选择“环境端口检查”，选择“环境端口检测”，点击“检查端口按钮”，如果出现以下内容，代表80端口没被占用，否则重启电脑重新运行程序：

请自行检查是否安装VC9运行库？？　重要！！
80和3306端口检测正常！

7. 关掉“环境端口检测”窗口，点击“其他选项菜单”按钮，选择“站点域名管理”选项.
8. 在网站域的后面填写本地网站域名，可以随便取一个，然后将第二域的后面的方框内的文字删除，然后点击“新增”按钮，其次点击“保存设置并生成配置文件”按钮，点击确定.
9. 点击“其他选项菜单”按钮，选择“网站根目录”选项，在弹出窗口中的文件全部删除，并且将“开户网页源码资源”文件夹内所有文件复制到根目录下.
10. 点击“其他选项菜单”按钮，选择“打开 host”选项，在弹出的记事本中插入新的一行，写入以下内容，然后保存：

127.0.0.2 （这里填写你刚才取的域名）

11. 点击程序上方的“启动”按钮，确保运行状态全绿，否则重启电脑.
12. 打开OpenFrp官网：https://www.openfrp.net
13. 注册一个新用户
14. 进入个人中心，点击签到，每天都要签，这样流量才会多
15. 点击控制台的新建隧道，选择不需要实名认证的，然后随机名称和端口号，本地IP修改为127.0.0.2，端口号填80，然后再新建一个隧道，最好是换一个节点，和刚才一样，但要把80改成3389
16. 创建好以后点配置文件，选刚才创建的节点，复制配置文件内容到frpc.ini，如果有第二个和刚才不一样的节点，那就在Frp文件夹下面放一个frpc1.ini，复制另外一个节点的配置文件内容到里面，最多到frpc10.ini， 
17. 然后右键打开start.bat，会弹出1～11个黑色窗口，具体几个看你弄了几个frpc.ini 
18. 任务最小化所有黑窗口
19. 如果需要开机自启的话，把你自己的administrator设置为自动登录，然后把Frp文件夹复制到C:\Users\administrator\AppData\Roaming\Microsoft\Windows\开始菜单\程序\启动，完成

PS:由于本人这次是退出了此行业来修订的，一些特殊的没写出来，程序源码归@afan所有，如果有问题，请及时联系删除！！！